<?php

namespace Thoth\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use ZipStream\ZipStream;
use Thoth\Http\Controllers\Controller;
use Thoth\Models\Document;
use Thoth\Models\Numeral;

class DocumentController extends Controller 
{

    public function tags()
    {
        $tags = Document::keys(Document::couch()->group(true)->getView(Document::viewId(), 'tags')->rows);
        return response()->json(
            $tags,
            200
        );
    }

    public function tag(String $tag)
    {
        return Document::view('tag', $tag);
    }

    public function years()
    {
        return Numeral::years();
    }

    public function year($year)
    {
        return Document::year($year);
    }

    public function search(String $query)
    {
        return Document::search($query);
    }

    public function document( $id)
    {
        return response()->json(Document::find($id));
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->rules());
        $doc = Document::find($id);
        try{
            $doc->update($request->all());
            return response()->json( $doc, 200 );
        }catch(\Exception $e){
            return response()->json( ['message'=>$e->getMessage()], 500 );
        }
    }

    public function download(Request $request, $id, $fileIndex = null)
    {
        $doc = Document::find($id);
        if($fileIndex){
            $file = $doc->attachments[$fileIndex];
            return response()->download($file->path, $file->filename, ['filename'=>$file->filename]);
        }else{
            $zip = new ZipStream('archive.zip');
            $files = $doc->attachments;
            foreach ($files as $file) {
                $zip->addFileFromPath($file->path, $file->filename);
            }
            return response()->streamDownload(function () use ($zip){
                $zip->finish();
            }, 'archive.zip', ['filename'=>'archive.zip']);
        }
    }

    public function delete(Request $request, $id)
    {
        $doc = Document::find($id);
        if(isset($doc->number)){
            return response()->json( ['message'=>'The document has been registered, cannot delete!'], 406 );
        }
        try {
            $doc->delete();
            return response()->json( [], 200 );
        }catch(\Exception $e){
            return response()->json( ['message'=>$e->getMessage()], 500 );
        }
    }

    public function rules(){
        return [];
    }
}