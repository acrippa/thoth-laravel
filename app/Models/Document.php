<?php

namespace Thoth\Models;

use Elasticquent\ElasticquentTrait;

class Document extends Model
{

    static $index = ["sender", "recipient", "mail_from", "subject", "mail_to"];
    static $fields = ["sender", "recipient", "subject", "body", "tags", "mail_from", "mail_to"];

    public static function year($year)
    {
        return self::_search([
            'match' => [
                'year' => $year
            ]
        ]);
    }

    public static function search($query)
    {
        return self::_search([
            "multi_match" => [
                "query" => $query,
                "fields" => self::$fields
            ]
        ]);
    }

    public function update($data)
    {
        $values = [];
        if(isset($data['register']) && !isset($this->number)){
            $values['date'] = date('Y-m-d');
            $values['year'] = date('Y');
            $values['time'] = date('H:i');
            $values['direction'] = $data['direction'];
            $values['number'] = $Numeral::number($this->direction, $this->year);
        }
        foreach(self::$fields as $field){
            $values[$field] = $data[$field];
        }
        try{
            $this->store($values);
        }catch(\Exception $ex){
            throw $ex;
        }
    }
}