<?php

namespace Thoth\Models;

use Elasticsearch\ClientBuilder;
use Illuminate\Support\Str;

class Model
{

    public function getTYpe()
    {
        if (! isset($this->type)) {
            return str_replace(
                '\\', '', Str::snake(class_basename($this))
            );
        }
        return $this->type;
    }

    public static function elastic()
    {
        $elastic = config('elastic');
        $hosts = [[
            'host' => $elastic['host'],
            'port' => $elastic['port'],
            'scheme' => $elastic['scheme'],
            'user' => $elastic['username'],
            'pass' => $elastic['password']
        ]];
        return ClientBuilder::create()
        ->setHosts($hosts)
        ->build();
    }

    public static function find($id)
    {
        $obj = new static();
        $params = [
            'index' => config('elastic')['index'],
            'type' => $obj->getType(),
            'id' => $id
        ];
        $doc = self::elastic()->get($params);
        foreach ($doc['_source'] as $key => $value) {
            $obj->{$key} = $value;
        }
        $obj->_id = $id;
        return $obj;
    }

    public static function _search($query)
    {
        $params = [
            'index' => config('elastic')['index'],
            'type' => (new static())->getType(),
            "body" => [
                "query" => $query
            ]
        ];
        $result = self::elastic()->search($params);
        $collection = [];
        if ($result['hits']['total'] > 0){
            foreach($result['hits']['hits'] as $hit){
                $hit['_source']['_id']=$hit['_id'];
                $collection[] = $hit['_source'];
            }
        }
        return collect($collection);
    }

    public function store($data)
    {
        $params = [
            'index' => config('elastic')['index'],
            'type' => $this->getType(),
            'id' => $this->_id,
            'body' => [
                'doc' => $data
            ]
        ];
        foreach($data as $key => $value){
            $this->{$key} = $value;
        }
        $response = $client->update($params);
    }

    public function __get($property)
    {
        if(is_callable(array($this, $property))) {
            return call_user_func(array($this, $property));
        }
    }


    public function delete()
    {
        $params = [
            'index' => config('elastic')['index'],
            'type' => $this->getType(),
            'id' => $this->_id
        ];
        self::elastic()->delete($params);
    }



}