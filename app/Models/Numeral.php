<?php

namespace Thoth\Models;

use Illuminate\Database\Eloquent\Model;

class Numeral extends Model
{

    public $increments = false;
    protected $primaryKey = 'year';

    const LOCKFILE = '/.lock';

    public static function number($direction, $year)
    {
        try{
            $number = self::where([
                ['year', $year],
                ['direction', $direction]
            ])->findOrFail();
            $number->number++;

        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $ex){
            $number = new self();
            $number->year = $year;
            $number->direction = $direction;
            $number->number = 1;
        }
        $number->save();
        return $number->number;
    }

    public static function years()
    {
        return self::distinct('year')->get('year');
    }

}