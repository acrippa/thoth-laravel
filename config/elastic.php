<?php

return [
    'host' => env('ELASTIC_HOST', 'localhost'),
    'port' => env('ELASTIC_PORT', '9200'),
    'scheme' => env('ELASTIC_SCHEME', 'http'),
    'username' => env('ELASTIC_USERNAME', ''),
    'password' => env('ELASTIC_PASSWORD', ''),
    'index' => env('ELASTIC_INDEX', 'thoth'),
];