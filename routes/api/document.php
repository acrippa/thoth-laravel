<?php 

Route::group([
    'prefix' => 'v1/documents', 
    'namespace' => 'Api\V1', 
    'middleware' => ['auth:api']
], function() {
    Route::get('/tags', [
        'as' => 'document.tags', 'uses' => 'DocumentController@tags'
    ]);
    Route::get('/tag/{tag}', [
        'as' => 'document.tag', 'uses' => 'DocumentController@tag'
    ]);
    Route::get('/years', [
        'as' => 'document.years', 'uses' => 'DocumentController@years'
    ]);
    Route::get('/year/{year}', [ 
        'as' => 'document.year', 'uses' => 'DocumentController@year'
    ]);
    Route::get('/search/{query}', [ 
        'as' => 'document.year', 'uses' => 'DocumentController@search'
    ]);
    Route::get('/{id}', [
        'as' => 'document.document', 'uses' => 'DocumentController@document'
    ]);
    Route::get('/{id}/download/{index}', [
        'as' => 'document.download', 'uses' => 'DocumentController@download'
    ]);
    Route::get('/{id}/download', [
        'as' => 'document.downloadAll', 'uses' => 'DocumentController@download'
    ]);
    Route::post('/', [
        'as' => 'document.create', 'uses' => 'DocumentController@create'
    ]);
    Route::put('/{document}', [
        'as' => 'document.update', 'uses' => 'DocumentController@update'
    ]);
    Route::delete('/{document}', [
        'as' => 'document.delete', 'uses' => 'DocumentController@delete'
    ]);
});